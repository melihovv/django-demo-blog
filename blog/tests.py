from django.test import TestCase, Client
from .models import Post
from django.core.urlresolvers import reverse


class PostTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_post_list(self):
        post_1 = Post.objects.create(title='post_1', text='abc')
        post_2 = Post.objects.create(title='post_2', text='ab def')

        url = reverse('post_list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/post_list.html')
        self.assertEqual(list(response.context['posts']), [post_2, post_1])

        response = self.client.get(url + '?query=abc')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.context['posts']), [post_1])
        self.assertEqual(response.context['query'], 'abc')
