from django.shortcuts import render_to_response
from .models import Post


def post_list(request):
    posts = Post.objects.all()
    query = request.GET.get('query')
    if query:
        posts = posts.filter(text__icontains=query)
    return render_to_response('blog/post_list.html', {
        'posts': posts,
        'query': query
    })


def post_detail(request, pk):
    post = Post.objects.get(pk=pk)
    return render_to_response('blog/post_detail.html', {'post': post})
